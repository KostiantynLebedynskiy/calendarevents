<?php

namespace App\Http\Controllers\Admin;

use App\Events;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.events.index');
    }
    public function indexInner($id)
    {

        $events = Events::where('user_id', auth()->user()->id)->where('id', $id)->get();
        return view('admin.events.inner', [
            'data' => response()->json($events)
        ]);
    }

    /**
     * Display all events by user ID
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function showAll(Request $request)
    {
        try {
            $sort = ($request->input('sort') ? $request->input('sort') : 'asc');
            $orderby = ($request->input('orderby') ? $request->input('orderby') : 'endEvent');
            $events = Events::where('user_id', auth()->user()->id)->orderBy($orderby, $sort)->get();
            return response()->json($events);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
    /**
     * Display all events by user ID
     *
     * @param  int  $id
     */
    public function showOne($id)
    {
        if (!$id) {
            throw new Exception('ID not found');
        }
        try {
            $events = Events::where('user_id', auth()->user()->id)->where('id', $id)->get();
            return response()->json($events);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
    /**
     * Display all events by user ID
     * @param  int  $id
     */
    public function deleteEvent($id)
    {
        if (!$id) {
            throw new Exception('ID not found');
        }
        try {
            $events = Events::where('user_id', auth()->user()->id)->where('id', $id)->delete();
            return response()->json(['status' => 200, 'msg' => 'Record deleted successfully']);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
    /**
     * Search events
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function searchEvents(Request $request)
    {
        try {
            $sort = ($request->input('sort') ? $request->input('sort') : 'asc');
            $orderby = ($request->input('orderby') ? $request->input('orderby') : 'endEvent');
            $search = ($request->input('search') ? $request->input('search') : '');
            if(!$search) {
                $events = Events::where('user_id', auth()->user()->id)->orderBy($orderby, $sort)->get();
            }
            else {
                $events = Events::where('user_id', auth()->user()->id)->where('nameEvent', 'like', '%' . $search . '%')->orderBy($orderby, $sort)->get();
            }

            return response()->json($events);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
    /**
 * Update event by ID
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 */
    public function updateEvent(Request $request, $id)
    {
        if (!$id) {
            throw new Exception('ID not found');
        }
        try {
            $data = [
                'nameEvent' => $request->input('nameEvent'),
                'descEvent' => $request->input('descEvent'),
                'startEvent' => $request->input('startEvent'),
                'endEvent' => $request->input('endEvent')
            ];
            $events = Events::where('user_id', auth()->user()->id)->where('id', $id)->update($data);
            if($events) {
                $events = Events::where('user_id', auth()->user()->id)->where('id', $id)->get();
                return response()->json($events);
            }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
    /**
     * Add event
     * @param  \Illuminate\Http\Request  $request
     */
    public function putEvent(Request $request)
    {
        try {
            $data = [
                'nameEvent' => $request->input('nameEvent'),
                'descEvent' => $request->input('descEvent'),
                'startEvent' => $request->input('startEvent'),
                'endEvent' => $request->input('endEvent'),
                'user_id' => auth()->user()->id
            ];
            $events = Events::where('user_id', auth()->user()->id)->insert($data);
            if($events) {
                $events = Events::where('user_id', auth()->user()->id)->get();
                return response()->json($events);
            }

        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}
