import Axios from 'axios';

const state = {
    eventsData: [],
};

const getters = {
    eventsData: state => {
        return state.eventsData;
    },
};

const mutations = {
    SET_EVENT: (state, payload) => {
        state.eventsData = payload;
    },
};

const actions = {
    SAVE_EVENT: async (context, payload) => {
        context.commit('SET_EVENT', payload);
    },
};

export default {
    state,
    getters,
    mutations,
    actions,
};
