/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueRouter from 'vue-router';
import moment from 'moment';
import {store} from './components/store';
//import 'moment/locale/uk';
Vue.use(Antd);
Vue.use(VueRouter);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/innerEvent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/innerEvent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// 3. Создаём экземпляр маршрутизатора и передаём маршруты в опции `routes`
// Вы можете передавать и дополнительные опции, но пока не будем усложнять.
import inner from './components/views/innerEvent.vue';
import events from './components/views/Calendar.vue';

Vue.component('calendar-events', require('./components/views/Calendar.vue').default);
Vue.component('events-inner', require('./components/views/innerEvent.vue').default);
const routes = [
    {
        path: '/admin/events/:id',
        name: 'inner',
        component: inner
    },
    {
        path: '/admin/events',
        name: 'events',
        component: events
    },
];

const router = new VueRouter({ mode: 'history', routes });
const app = new Vue({ router, store }).$mount('#app');
