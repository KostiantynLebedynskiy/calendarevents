<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
    Route::get('events', 'EventsController@index')->name('admin.index');
    Route::get('events/{id}', 'EventsController@indexInner');
});
Route::group(['prefix'=>'api', 'namespace'=>'Admin', 'middleware'=>['auth']], function(){
    Route::get('events', 'EventsController@showAll');
    Route::get('events/{id}', 'EventsController@showOne');
    Route::get('events-search', 'EventsController@searchEvents');
    Route::delete('events/{id}', 'EventsController@deleteEvent');
    Route::put('events/{id}', 'EventsController@updateEvent');
    Route::post('events', 'EventsController@putEvent');
});
