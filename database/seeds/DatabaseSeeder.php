<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $data = [
            ['nameEvent'=>'Test 1', 'descEvent'=> 'it is a test 1', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s', strtotime(' - 3 days')), 'endEvent' => date('Y-m-d H:m:s', strtotime(' + 3 days'))],
            ['nameEvent'=>'Test 2', 'descEvent'=> 'it is a test 2', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s', strtotime(' - 1 days')), 'endEvent' => date('Y-m-d H:m:s', strtotime(' + 1 days'))],
            ['nameEvent'=>'Test 3', 'descEvent'=> 'it is a test 3', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s', strtotime(' - 2 days')), 'endEvent' => date('Y-m-d H:m:s', strtotime(' + 2 days'))],
            ['nameEvent'=>'Test 4', 'descEvent'=> 'it is a test 4', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s', strtotime(' - 4 days')), 'endEvent' => date('Y-m-d H:m:s', strtotime(' + 1 days'))],
            ['nameEvent'=>'Test 5', 'descEvent'=> 'it is a test 5', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s'), 'endEvent' => date('Y-m-d H:m:s')],
            ['nameEvent'=>'Test 6', 'descEvent'=> 'it is a test 5', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s'), 'endEvent' => date('Y-m-d H:m:s')],
            ['nameEvent'=>'Test 7', 'descEvent'=> 'it is a test 6', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s', strtotime(' - 5 days')), 'endEvent' => date('Y-m-d H:m:s', strtotime(' + 5 days'))],
            ['nameEvent'=>'Test 8', 'descEvent'=> 'it is a test 7', 'user_id' => 1,  'startEvent' => date('Y-m-d H:m:s', strtotime(' + 36 days')), 'endEvent' => date('Y-m-d H:m:s', strtotime(' + 38 days'))]
        ];
        DB::table('events')->insert($data);
    }
}
